<?php
use yii\helpers\Html;
use yii\widgets\LinkPager;
?>

<h1>Статьи</h1>

<div class="panel panel-default">
    <div class="panel-body">
        <form action="/article/search" method="post" class="navbar-form navbar-left">

            <div class="form-group">
                <div class="glyphicon glyphicon-search"></div>
                <input type="text" name="search" class="form-control" placeholder="Найти">
                <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            </div>

            <button type="submit" name="click" value="df" class="btn btn-default">Поиск</button>
        </form>

        <?php
        $cookies = Yii::$app->request->cookies;
        if (isset($cookies['name'])) { ?>

            <div class="text-right form-group">
                <a class="btn btn-primary" href="/article/entry">Добавить новость</a>
            </div><? } ?>
    </div>
</div>

<ul>


    <?php foreach ($articles as $article): ?>

        <div class="panel panel-default">
            <div class="panel-heading">

                <div class="col-md-5"><?= Html::encode("{$article->heading}") ?></div>
                <div class="text-right">
                    <b><?= $article->user->name ?></b>
                    <a class="label label-warning" href="/article/edit?id=<?= $article->id ?>">Редактировать</a>
                    <a class="label label-danger" href="/article/delete?id=<?= $article->id ?>">Удалить</a>
                </div>
            </div>
            <div class="panel-body">
                <?= Html::encode("{$article->message}") ?>

                <div class="text-right">
                    <?for($i=0; $i<count($article->tags); $i++):?>
                        <span class="label label-info" style="margin: 3px">
                            <a href="/article/tag-search?tag=<?=$article->tags[$i]->tagName?>">
                                <?=$article->tags[$i]->tagName?></a>
                        </span>
                    <?endfor;?>
                </div>

            </div>
        </div>

    <?php endforeach; ?>

</ul>

<?= LinkPager::widget(['pagination' => $pagination]) ?>

