<?php
use yii\helpers\Html;
?>
<p>Данная информация сохранена в БД:</p>

<ul>
    <li><label>Heading</label>: <?= Html::encode($model->heading) ?></li>
    <li><label>Message</label>: <?= Html::encode($model->message) ?></li>
</ul>

<div class="form-group">
    <a href="/article" class="btn btn-primary">На главную</a>

</div>