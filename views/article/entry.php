<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
    <?php
if(isset($model->id)){
    $form = ActiveForm::begin(['action' => 'update?id=' . $model->id]);
}else{
    $form = ActiveForm::begin();
}
?>

<?= $form->field($model, 'heading') ?>

<?= $form->field($model, 'message') ?>

<?= $form->field($model, 'tags') ?>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>