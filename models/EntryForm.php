<?php

namespace app\models;

use yii\base\Model;

class EntryForm extends Model
{
    public $heading;
    public $message;
    public $tags;

    public function rules()
    {
        return [
            [['heading', 'message', 'tags'], 'required'],
        ];
    }
}