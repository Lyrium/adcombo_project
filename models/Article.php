<?php

namespace app\models;

use yii\db\ActiveRecord;


class Article extends ActiveRecord
{
    public static function tableName()
    {
        return 'articles';
    }

    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('articles');
    }

    public function getTags()
    {
        return $this->hasMany(ArticlesTags::className(), ['article_id' => 'id']);
    }

}

