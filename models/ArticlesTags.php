<?php

namespace app\models;

use yii\db\ActiveRecord;


class ArticlesTags extends ActiveRecord
{
    public static function tableName()
    {
        return 'articles_tags';
    }

    public function rules()
    {
        return [
            [['tag_id', 'article_id'], 'required']
        ];
    }

    public function getTagName() {


        return Tags::findOne($this->tag_id)->tag_name;
    }
}