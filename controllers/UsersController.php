<?php

namespace app\controllers;

use app\models\Users;
use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\UsersForm;

class UsersController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionReg()
    {
        $model = new UsersForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // данные в $model удачно проверены
            $user = new Users();
            $user->name = $model->name;
            $user->password = md5($model->password);
            $user->authKey = 1;
            $user->accessToken = 1;
            $user->save();

            return $this->render('gut-reg', ['model' => $model]);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('form-reg', ['model' => $model]);
        }
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        #<form>
        $form = Yii::$app->request->post();
        $name = $form['LoginForm']['name'];
        $password = md5($form['LoginForm']['password']);

        #data base
        $user = Users::findOne(['name' => $name]);

        if ($user->name == $name && $user->password == $password) {

            $hash = md5($this->generateCode());

            // получение коллекции (yii\web\CookieCollection) из компонента "response"
            $cookies = Yii::$app->response->cookies;
            // добавление новой куки
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'id',
                    'value' => $user->id,
                ]));
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'name',
                    'value' => $name,
                ]));
                $cookies->add(new \yii\web\Cookie([
                    'name' => 'hash',
                    'value' => $hash,
                ]));

            //запись в базу
            $user->authKey = $hash;
            $user->save();

            return $this->goBack();
        } else
            return $this->render('login', ['model' => $model]);

    }

    # Функция для генерации случайной строки
    public function generateCode($length = 6)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
        $code = "";
        $clen = strlen($chars) - 1;
        while (strlen($code) < $length) {
            $code .= $chars[mt_rand(0, $clen)];
        }
        return $code;
    }

    public function actionLogout()
    {
        // получение коллекции (yii\web\CookieCollection) из компонента "response"
        $cookies = Yii::$app->response->cookies;
        // удаление куки...
        $cookies->remove('name');
        $cookies->remove('hash');
        return $this->goBack();
    }

}