<?php

namespace app\controllers;

use app\models\ArticlesTags;
use app\models\Tags;
use app\models\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Article;
use app\models\EntryForm;

class ArticleController extends Controller
{
    public function actionIndex()
    {
        $query = Article::find();

        $pagination = new Pagination([
            'defaultPageSize' => 8,
            'totalCount' => $query->count(),
        ]);

        $articles = $query
            ->with('user')
            ->with('tags')
            ->orderBy('heading')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

//        print_r($articles[16]->tags[0]->tagName); exit;

        return $this->render('index', [
            'articles' => $articles,
            'pagination' => $pagination,
        ]);
    }


    public function actionEntry()
    {
        $model = new EntryForm();
        $cookies = Yii::$app->request->cookies;
        $user_id = $cookies['id'];
        $user = Users::findOne($user_id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $article = new Article();
            $article->heading = $model->heading;
            $article->message = $model->message;
            $user->link('articles', $article);

            $tags = explode(',', $model->tags);

            foreach ($tags as $tag) {
                $tag = trim($tag);
                $oneTag = Tags::findOne(['tag_name' => $tag]);
                if(!$oneTag){
                    $oneTag = new Tags();
                    $oneTag->tag_name = $tag;
                    $oneTag->save();
                }

                $artTeg = new ArticlesTags();
                $artTeg->article_id = $article->id;
                $artTeg->tag_id = $oneTag->tag_id;
                $artTeg->save();
            }

            return $this->render('entry-confirm', ['model' => $model]);
        } else {
            // либо страница отображается первый раз, либо есть ошибка в данных
            return $this->render('entry', ['model' => $model]);
        }
    }

    public function actionDelete($id)
    {
        $article = Article::findOne($id);
        $article->delete();
        return $this->goBack();
    }

    public function actionEdit($id)
    {
        $article = Article::findOne($id);
        return $this->render('entry', ['model' => $article]);

    }

    public function actionUpdate($id)
    {
        $params = Yii::$app->request->post();

        $article = Article::findOne($id);
        $article->heading = $params['Article']['heading'];
        $article->message = $params['Article']['message'];
        $article->update();
        $this->redirect('/article');
    }

    public function actionSearch()
    {

        $strSearch = Yii::$app->request->post()['search'];
        $postSearch = $strSearch;
        $strSearch = explode(' ', $strSearch);

        foreach ($strSearch as $key => $row) {
            $strSearch[$key] = '*' . $row . '*';
        }

        $strSearch = implode(' ', $strSearch);

        $articles = Article::find()
            ->where('match(heading, message) against ("' . $strSearch . '" IN BOOLEAN MODE)');

        $provider = new ActiveDataProvider([
            'query' => $articles,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);

        return $this->render('search', compact(['articles', 'provider', 'postSearch']));
    }

    public function actionTagSearch($tag)
    {
        $articles = Article::find()
            ->leftJoin('tags', 'tags.tag_name = "' . $tag . '"')
            ->leftJoin('articles_tags', 'articles_tags.tag_id = tags.tag_id')
            ->where('articles.id = articles_tags.article_id')->all();

        $pagination = new Pagination([
            'defaultPageSize' => 8,
            'totalCount' => count($articles),
        ]);
        return $this->render('index', compact('articles', 'pagination'));
    }
}